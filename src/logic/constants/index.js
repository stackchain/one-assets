export const STATUS_INIT = "INIT";
export const STATUS_OK = "OK";
export const STATUS_FAIL = "FAIL";
export const STATUS_WAITING = "WAITING";
export const STATUS_LOADING = "LOADING";
export const STATUS_SUCCESS = "SUCCESS";
export const STATUS_ERROR = "ERROR";

export const WALLET_ETH_EDIT = "wallets/ETH_EDIT";
export const WALLET_ETH_EDIT_OK = "wallets/ETH_EDIT_OK";
export const WALLET_ETH_EDIT_FAIL = "wallets/ETH_EDIT_FAIL";

export const WALLET_ETH_GET = "wallets/ETH_GET";
export const WALLET_ETH_GET_OK = "wallets/ETH_GET_OK";
export const WALLET_ETH_GET_FAIL = "wallets/ETH_GET_FAIL";

export const OPTIONS_UPDATE = "options/UPDATE";

export const COMPONENT_WALLET = "wallet";
export const COMPONENT_MANAGER_KYC = "manager-kyc";
