import { all } from "redux-saga/effects";

import sagaHooks from "./hooks";
import sagaWalletETH from "./walletETH";

function* mySaga() {
  try {
    yield all([
      ...sagaHooks,
      ...sagaWalletETH
    ]);
  } catch (e) {
    console.error("Sagas not loaded this widget wont work at all", e);
  }
}

export default mySaga;