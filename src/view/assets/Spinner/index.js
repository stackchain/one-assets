import { h } from "preact";
import classNames from "classnames";

import style from "./style.css";

const Spinner = (props) => {
  return (
    <div className={classNames(style.loader, { [style.hide]: props.hide })}>
    </div>
  );
};

export default Spinner;