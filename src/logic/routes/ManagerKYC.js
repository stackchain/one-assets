import { h, Component } from "preact";
import { connect } from "react-redux";

import { edit } from "../reducers/walletETH/edit";
import { get } from "../reducers/walletETH/get";
import { ViewManagerKYC } from "../../view/components";
import { eth } from "../utils";
import { sendEvent, sendError } from "../../tracker";
import { isLoading } from "../selectors/walletETH";

import * as C from "../constants";

const mapStateToProps = state => ({
  walletETHGet: state.walletETHGet,
  walletETHEdit: state.walletETHEdit,
  isLoading: isLoading(state)
});

const mapDispatchToProps = dispatch => ({
  edit: data => dispatch(edit(data)),
  get: id => dispatch(get({ id }))
});

class RouteManagerKYC extends Component {

  constructor(props) {
    super(props);
    
    this.handleSaveWallet = this.handleSaveWallet.bind(this);
    this.handleClickCreate = this.handleClickCreate.bind(this);
    this.successOverlay = this.successOverlay.bind(this);
    
    this.state = {
      status: C.STATUS_INIT,
      overlay: {
        status: C.STATUS_INIT,
        loadingMsg: "",
        successMsg: "",
        successBtnLabel: "",
        successCb: this.successOverlay,
        errorMsg: "",
        errorBtnLabel: "",
        errorCb: () => {},
        hasConfirmation: [C.STATUS_SUCCESS, C.STATUS_ERROR],
        confirmationTimeout: 5
      },
      wallets: {
        ETH: []
      },
      invalidWallets: {
        ETH: []
      }
    };
  }

  componentDidMount() {
    this.props.get(this.props.id);
  }

  componentDidUpdate(prevProps) {
    // any request happens
    if (prevProps.isLoading !== this.props.isLoading) {
      // start spinner
      if (this.props.isLoading) {
        this.setState({
          overlay: {
            ...this.state.overlay,
            status: C.STATUS_LOADING
          }
        });
      }
      // get happens?
      if (prevProps.walletETHGet.status !== this.props.walletETHGet.status) {
        switch (this.props.walletETHGet.status) {
          case C.STATUS_OK:
            this.setState({
              wallets: this.props.walletETHGet.data.wallets,
              overlay: {
                ...this.state.overlay,
                status: C.STATUS_INIT
              }
            });
            break;
          case C.STATUS_FAIL:
            this.setState({
              overlay: {
                ...this.state.overlay,
                errorMsg: this.props.i18n.t("wallet.overlay.get.error.message"),
                errorBtnLabel: this.props.i18n.t("wallet.overlay.get.error.button_label"),
                errorCb: () => this.props.get(this.props.id),
                status: C.STATUS_ERROR
              }
            });
            break;
        }
      }
      // edit happens?
      if (prevProps.walletETHEdit.status !== this.props.walletETHEdit.status) {
        switch (this.props.walletETHEdit.status) {
          case C.STATUS_OK:
            this.setState({
              overlay: {
                ...this.state.overlay,
                successMsg: this.props.i18n.t("wallet.overlay.edit.success.message"),
                successBtnLabel: this.props.i18n.t("wallet.overlay.edit.success.button_label"),
                status: C.STATUS_SUCCESS
              }
            });
            break;
          case C.STATUS_FAIL:
            this.setState({
              overlay: {
                ...this.state.overlay,
                errorMsg: this.props.i18n.t("wallet.overlay.edit.error.message"),
                errorBtnLabel: this.props.i18n.t("wallet.overlay.edit.error.button_label"),
                status: C.STATUS_ERROR
              }
            });
            break;
        }
      }
    }
    if (prevProps.id !== this.props.id) {
      this.props.get(this.props.id);
    }
  }

  successOverlay() {
    this.setState({
      overlay: {
        ...this.state.overlay,
        status: C.STATUS_INIT
      }
    });
  }

  handleSaveWallet() {
    if (!eth.isValid(this.state.wallets.ETH[0])) {
      sendError("User input wrong wallet address");
      this.setState({
        invalidWallets: { ETH: [true] }
      });
    } else {
      this.props.edit({
        id: this.props.id,
        wallets: this.state.wallets
      });
    }
  }

  handleClickCreate() {
    sendEvent("Looking for help to create wallet");
  }

  render() {
    const {
      i18n,
      readOnly
    } = this.props;

    return (
      <ViewManagerKYC
        onChangeField={e => {
          this.setState({
            wallets: { ETH: [e] },
            invalidWallets: { ETH: [!eth.isValid(e)] }
          });
        }}
        onSaveWallet={this.handleSaveWallet}
        onCreate={this.handleClickCreate}
        wallet={this.state.wallets.ETH[0]}
        invalidWallet={this.state.invalidWallets.ETH[0]}
        readOnly={readOnly}
        overlay={this.state.overlay}
        i18n={i18n}
      />
    );
  }

}

export default connect(mapStateToProps, mapDispatchToProps)(RouteManagerKYC);