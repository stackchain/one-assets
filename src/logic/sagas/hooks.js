import { select, takeEvery } from "redux-saga/effects";

function* hook(action) {
  let state = {};
  let f = null;
  try {
    state = yield select();
    f = state.hooks[action.type];
  } catch (e) {
    console.error("Selector failed:", e);
  }

  // TODO: unsafe so far - it's gonna be better in the future :D
  // perhaps using yield spawn - for non-blocking make it configurable from hooks obj dunno atm.
  if (typeof f === "function") {
    try {
      f(state, action);
    } catch (e) {
      console.error("Hook up failed:", action.type, e);
    }
  }

}

export default [
  takeEvery("*", hook)
];