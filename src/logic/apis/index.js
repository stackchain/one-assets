const fetchDefaults = {
  mode: "cors",
  headers: {
    "Access-Control-Allow-Origin": "*",
    "Content-Type": "application/json; charset=utf-8"
  }
};

const endpoint = process.env.API_ENDPOINT;
const resourceUser = "users";

export const walletGet = ({ id }) =>
  fetch(`${endpoint}/${resourceUser}/${id}`, {
    ...fetchDefaults
  }).then(response => {
    if (response.ok) return response.json();
    throw new Error(`${response.status} ${response.statusText}`);
  });

export const walletPut = ({ id, wallets }) =>
  fetch(`${endpoint}/${resourceUser}/${id}`, {
    ...fetchDefaults,
    method: "PUT",
    body: JSON.stringify({ wallets })
  }).then(response => {
    if (response.ok) return response.json();
    throw new Error(`${response.status} ${response.statusText}`);
  });