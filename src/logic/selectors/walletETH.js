import { createSelector } from "reselect";

import * as C from "../constants";

const isGetting = state => state.walletETHGet.status === C.STATUS_WAITING;
const isSaving = state => state.walletETHEdit.status === C.STATUS_WAITING;

export const isLoading = createSelector(
  [isGetting, isSaving],
  (getting, saving) => (getting || saving)
);