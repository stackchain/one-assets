import webpack from "webpack";
import HtmlWebpackPlugin from "html-webpack-plugin";
import autoprefixer from "autoprefixer";
import customMedia from "postcss-custom-media";
import url from "postcss-url";
import ExtractTextPlugin from "extract-text-webpack-plugin";
import UglifyJSPlugin from "uglifyjs-webpack-plugin";
import SpeedMeasurePlugin from "speed-measure-webpack-plugin";
import { BundleAnalyzerPlugin } from "webpack-bundle-analyzer";
import mapObject from "object-loops/map";
import mapKeys from "object-loops/map-keys";

import packageJson from "./package.json";

const API_ONE_VERSION = "v1";
const SDK_VERSION = packageJson.version;
const ENV = process.env.NODE_ENV || "production";
const PRODUCTION_BUILD = ENV !== "development";
const WEBPACK_ENV = PRODUCTION_BUILD ? "production" : "development";
const DEV_OR_STAGING = ENV === "staging" || ENV === "development";

const PROD_CONFIG = {
  "API_ENDPOINT": `https://api.onepercent.io/${API_ONE_VERSION}`,
  "PUBLIC_PATH": `https://assets.onepercent.io/${SDK_VERSION}/`
};

const TEST_CONFIG = {
  ...PROD_CONFIG,
  "PUBLIC_PATH": "/"
};

const STAGING_CONFIG = {
  "API_ENDPOINT": `https://staging-api.onepercent.io/${API_ONE_VERSION}`,
  "PUBLIC_PATH": "/"
};

const DEV_CONFIG = {
  "API_ENDPOINT": `http://localhost:5001/staging-one-api/us-central1/api/${API_ONE_VERSION}`,
  "PUBLIC_PATH": "/"
};

const CONFIG_MAP = {
  development: DEV_CONFIG,
  staging: STAGING_CONFIG,
  test: TEST_CONFIG,
  production: PROD_CONFIG,
};

const CONFIG = CONFIG_MAP[ENV];

const formatDefineHash = defineHash =>
  mapObject(
    mapKeys(defineHash, key => `process.env.${key}`),
    value => JSON.stringify(value)
  );

const basePlugins = (bundleName) => ([
  new BundleAnalyzerPlugin({
    analyzerMode: "static",
    openAnalyzer: false,
    reportFilename: `${__dirname}/dist/reports/bundle_${bundleName}_size.html`,
    defaultSizes: "parsed"
  }),
  new webpack.NoEmitOnErrorsPlugin(),
  new webpack.DefinePlugin(formatDefineHash({
    API_ONE_VERSION,
    "NODE_ENV": WEBPACK_ENV,
    "API_ENDPOINT": CONFIG.API_ENDPOINT,
    SDK_VERSION,
    "BASE_32_VERSION": "AG",
    "WOOPRA_DOMAIN": `${DEV_OR_STAGING ? "":""}onepercent.io`
  }))
]);

const baseRules = [
  {
    test: /\.jsx?$/,
    include: [
      `${__dirname}/src`
    ],
    use: ["babel-loader"]
  },
  // webpack 4 handles JSON files natively
  // {
  //   test: /\.json$/,
  //   use: ["json-loader"]
  // },
  {
    test: /\.(xml|txt|md)$/,
    use: ["raw-loader"]
  }
];

const baseStyleLoaders = (modules=true) => [
  //ref: https://github.com/unicorn-standard/pacomo The standard used for naming the CSS classes
  //ref: https://github.com/webpack/loader-utils#interpolatename The parsing rules used by webpack
  {
    loader: "css-loader",
    options: {
      sourceMap: true,
      modules,
      localIdentName: "one-web-widgets-[folder]-[local]"
    }
  },
  {
    loader: "postcss-loader",
    options: {
      plugins: () => [
        customMedia(),
        autoprefixer({ browsers: "last 2 versions" }),
        url({ url: "inline" })
      ],
      sourceMap: true
    }
  },
  {
    loader: "less-loader",
    options: {
      sourceMap: true
    }
  }
];

const baseStyleRules = (disableExtractToFile = false) =>
  [{
    rule: "exclude",
    modules: true
  },
  {
    rule: "include",
    modules: false
  }].map(({rule, modules})=> ({
    test: /\.(less|css)$/,
    [rule]: [`${__dirname}/node_modules`],
    use: disableExtractToFile ?
      ["style-loader",...baseStyleLoaders(modules)] :
      ExtractTextPlugin.extract({
        fallback: "style-loader",
        use: baseStyleLoaders(modules)
      })
  }));

const baseConfig = {
  mode: process.env.NODE_ENV || "production",

  context: `${__dirname}/src`,
  entry: "./index.js",

  resolve: {
    extensions: [".jsx", ".js", ".json", ".less"],
    modules: [
      `${__dirname}/node_modules`,
      `${__dirname}/src`
    ],
    alias: {
      "react": "preact-compat",
      "react-dom": "preact-compat",
    }
  },

  stats: { colors: true },

  node: {
    global: true,
    process: false,
    Buffer: false,
    __filename: false,
    __dirname: false,
    setImmediate: false
  },

  devtool: PRODUCTION_BUILD ? "source-map" : "eval-source-map"

};

const configFinal = {
  ...baseConfig,

  output: {
    library: "OneWebWidgets",
    libraryTarget: "umd",
    path: `${__dirname}/dist`,
    publicPath: CONFIG.PUBLIC_PATH,
    filename: "one-web-widgets.min.js",
    chunkFilename: "one-web-widgets.[name].min.js"
  },

  module: {
    rules: [
      ...baseRules,
      ...baseStyleRules(),
      {
        test: /\.(svg|woff2?|ttf|eot|jpe?g|png|gif)(\?.*)?$/i,
        use: ["file-loader?name=images/[name]_[hash:base64:5].[ext]"]
      },
      {
        test: /\.html$/,
        use: ["html-loader?interpolate"]
      }
    ]
  },

  plugins: [
    ...basePlugins("dist"),
    new ExtractTextPlugin({
      filename: "style.css",
      allChunks: true,
      disable: !PRODUCTION_BUILD
    }),
    new HtmlWebpackPlugin({
      template: "./index.ejs",
      minify: { collapseWhitespace: true },
      inject: "body",
    }),
    ... PRODUCTION_BUILD ?
      [
        new UglifyJSPlugin({
          sourceMap: true,
          uglifyOptions: {
            compress: {
              pure_getters: true,
              unsafe: true,
              warnings: false,
            },
            output: {
              beautify: false,
            }
          }
        }),
        new webpack.LoaderOptionsPlugin({
          minimize: true,
          debug: false
        })
      ] : []
  ],

  devServer: {
    port: process.env.PORT || 8080,
    host: "0.0.0.0",
    publicPath: "/",
    contentBase: "./dist",
    historyApiFallback: true,
    disableHostCheck: true // IE with VBOX, since it goes through a proxy, see: https://github.com/webpack/webpack-dev-server/issues/882
  }
};

const configNpmLib = {
  ...baseConfig,
  name: "npm-library",
  output: {
    libraryTarget: "commonjs2",
    path: `${__dirname}/lib`,
    filename: "index.js"
  },
  module: {
    rules: [
      ...baseRules,
      ...baseStyleRules(true)
    ]
  },
  plugins: [
    ...basePlugins("npm"),
    new webpack.optimize.LimitChunkCountPlugin({
      maxChunks: 1
    })
  ]
};

const smp = new SpeedMeasurePlugin();

export default [smp.wrap(configFinal), configNpmLib];
