import * as C from "../../constants";

const initialState = {
  status: C.STATUS_INIT,
  message: "",
  data: {
    wallets: {
      ETH: []
    }
  }
};

export default function reducer(state = initialState, action = {}) {
  switch (action.type) {
    case C.WALLET_ETH_GET:
      return {
        ...state,
        status: C.STATUS_WAITING,
        message: ""
      };
    case C.WALLET_ETH_GET_OK:
      return {
        ...state,
        status: C.STATUS_OK,
        data: action.payload,
        message: ""
      };
    case C.WALLET_ETH_GET_FAIL:
      return {
        ...state,
        status: C.STATUS_FAIL,
        message: action.message
      };
    default:
      return state;
  }
}

export function get(data) {
  return {
    type: C.WALLET_ETH_GET,
    payload: { ...data }
  };
}

export function getOk(data) {
  return {
    type: C.WALLET_ETH_GET_OK,
    payload: { ...data }
  };
}

export function getFail(error) {
  return {
    type: C.WALLET_ETH_GET_FAIL,
    message: error.message
  };
}