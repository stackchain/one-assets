import * as C from "../../constants";

const initialState = {
  status: C.STATUS_INIT,
  message: ""
};

export default function reducer(state = initialState, action = {}) {
  switch (action.type) {
    case C.WALLET_ETH_EDIT:
      return {
        ...state,
        status: C.STATUS_WAITING,
        message: ""
      };
    case C.WALLET_ETH_EDIT_OK:
      return {
        ...state,
        status: C.STATUS_OK,
        message: ""
      };
    case C.WALLET_ETH_EDIT_FAIL:
      return {
        ...state,
        status: C.STATUS_FAIL,
        message: action.message
      };
    default:
      return state;
  }
}

export function edit(data) {
  return {
    type: C.WALLET_ETH_EDIT,
    payload: { ...data }
  };
}

export function editOk() {
  return {
    type: C.WALLET_ETH_EDIT_OK
  };
}

export function editFail(error) {
  return {
    type: C.WALLET_ETH_EDIT_FAIL,
    message: error.message
  };
}