import { h, Component } from "preact";
import classNames from "classnames";

import { AssetOverlay as Overlay } from "../../assets";

class ViewWalletETH extends Component {

  constructor(props) {
    super(props);
  }

  render () {
    const {
      readOnly,
      wallet,
      invalidWallet,
      onChangeField,
      onSaveWallet,
      onCreate,
      overlay,
      i18n
    } = this.props;

    return (
      <div className="card mb-2 shadow-sm">
        <div className="card-body">
          <p className="card-text">{i18n.t("wallet.title")}</p>
          <form>
            <div>
              <input type="text" className={classNames({ "is-invalid": invalidWallet }, "form-control form-control-sm")} id="wallet" placeholder={i18n.t("wallet.placeholder")} value={wallet} disabled={readOnly} onChange={e => onChangeField(e.target.value)}/>
              <div className="invalid-feedback">
                {i18n.t("wallet.errors.invalid_wallet")}
              </div>
              <div className="d-flex justify-content-between align-items-center">
                { readOnly ?
                  <small className="text-muted">{i18n.t("wallet.message_filled")}</small>
                  :
                  <small className="text-muted">{i18n.t("wallet.message_not_filled")}</small>
                }
              </div>
              <hr/>
              <div className="d-flex flex-row justify-content-end align-items-center">
                <a href="https://kb.myetherwallet.com/getting-started/creating-a-new-wallet-on-myetherwallet.html" className="btn btn-outline-secondary btn-sm" onClick={onCreate}>{i18n.t("wallet.button_create")}</a>
                <button className="btn btn-primary btn-sm ml-1" type="button" disabled={readOnly || invalidWallet} onClick={onSaveWallet}>{i18n.t("wallet.button_save")}</button>
              </div>
            </div>
          </form>
        </div>
        <Overlay {...overlay} />
      </div>
    );
  }

}

export default ViewWalletETH;