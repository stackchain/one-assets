import RouteWalletETH from "./WalletETH";
import RouteManagerKYC from "./ManagerKYC";
import RouteApp from "./App";

export default {
  RouteApp,
  RouteWalletETH,
  RouteManagerKYC
};