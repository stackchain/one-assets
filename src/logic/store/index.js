import {
  createStore,
  applyMiddleware,
  compose,
  combineReducers
} from "redux";
import createSagaMiddleware from "redux-saga";

import walletETHEdit from "../reducers/walletETH/edit";
import walletETHGet from "../reducers/walletETH/get";
import sagas from "../sagas";

export default function(hookUp) {
  const hooks = (state = {}) => ({ ...state, ...hookUp });
  const reducer = combineReducers({
    walletETHEdit,
    walletETHGet,
    hooks
  });

  const sagaMiddleware = createSagaMiddleware();

  const composeEnhancers =
    typeof window === "object" &&
    window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ ?
      window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__({
      // Specify extension’s options like name, actionsBlacklist, actionsCreators, serialize...
      }) : compose;

  const enhancer = composeEnhancers(
    applyMiddleware(sagaMiddleware)
  );
      
  const store = createStore(
    reducer,
    undefined,
    enhancer,
    typeof window !== "undefined" && window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
  );
  
  sagaMiddleware.run(sagas);

  return store;
}