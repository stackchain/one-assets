import { h, Component } from "preact";
import PropTypes from "prop-types";
import classNames from "classnames";

import Spinner from "../Spinner";
import Countdown from "../Countdown";
import * as C from "../../../logic/constants";

import style from "./style.css";

class Overlay extends Component {
  constructor(props) {
    super(props);

    this.waitConfirmation = this.waitConfirmation.bind(this);

    this.state = {
      show: false
    };
  }

  static propTypes = {
    status: PropTypes.oneOf([
      C.STATUS_ERROR,
      C.STATUS_SUCCESS,
      C.STATUS_LOADING,
      C.STATUS_WAITING,
      C.STATUS_INIT
    ]),
    loadingMsg: PropTypes.string,
    successMsg: PropTypes.string,
    successBtnLabel: PropTypes.string,
    successCb: PropTypes.func.isRequired,
    errorMsg: PropTypes.string,
    errorBtnLabel: PropTypes.string,
    errorCb: PropTypes.func.isRequired,
    hasConfirmation: PropTypes.array,
    confirmationTimeout: PropTypes.number
  };

  static defaultProps = {
    status: C.STATUS_INIT,
    successBtnLabel: "OK",
    errorBtnLabel: "OK",
    hasConfirmation: [],
    confirmationTimeout: 0
  };

  waitConfirmation(list, status) {
    return list.find((v) => v === status);
  }

  componentDidUpdate(prevProps) {
    if (prevProps.status !== this.props.status) {
      switch (this.props.status) {
        case C.STATUS_LOADING:
        case C.STATUS_SUCCESS:
        case C.STATUS_WAITING:
        case C.STATUS_ERROR:
          this.setState({ show: true });
          break;
        case C.STATUS_INIT:
          this.setState({ show: false });
          break;
      }
      return true;
    }
  }

  render() {
    const {
      status,
      loadingMsg,
      successMsg,
      successBtnLabel,
      successCb,
      errorMsg,
      errorBtnLabel,
      errorCb,
      hasConfirmation,
      confirmationTimeout,
    } = this.props;

    const {
      show
    } = this.state;

    return (
      <div className={classNames(style.overlay, { [style.show]: show })}>
        { status === C.STATUS_SUCCESS &&
          <div className="d-flex h-100 flex-column align-items-center justify-content-center">
            <div className={classNames(style.puff, "d-flex flex-column align-items-center justify-content-center")}>
              <i className="text-success fas fa-check fa-2x"></i>
            </div>
            <small className="text-white mt-2">{successMsg}</small>
            { this.waitConfirmation(hasConfirmation, status) && <button className="btn btn-primary btn-sm mt-4" onClick={successCb}>{successBtnLabel} { confirmationTimeout &&
              <Countdown seconds={confirmationTimeout} action={successCb}/> }
            </button> }
          </div>
        }
        { status === C.STATUS_ERROR &&
          <div className="d-flex h-100 flex-column align-items-center justify-content-center">
            <div className={classNames(style.puff, "d-flex flex-column align-items-center justify-content-center")}>
              <i className="text-danger fas fa-times fa-2x"></i>
            </div>
            <small className="text-white mt-2">{errorMsg}</small>
            { this.waitConfirmation(hasConfirmation, status) && <button className="btn btn-primary btn-sm mt-4" onClick={errorCb}>{errorBtnLabel} { confirmationTimeout &&
              <Countdown seconds={confirmationTimeout} action={errorCb}/> }
            </button> }
          </div>
        }
        { status === C.STATUS_LOADING &&
          <div className="d-flex h-100 flex-column align-items-center justify-content-center">
            <Spinner hide={status !== C.STATUS_LOADING} />
            <small className="text-white mt-2">{loadingMsg}</small>
          </div>
        }
      </div>
    );
  }
}

export default Overlay;