import ViewWalletETH from "./WalletETH";
import ViewManagerKYC from "./ManagerKYC";

export default {
  ViewWalletETH,
  ViewManagerKYC
};