import { h, Component } from "preact";
import PropTypes from "prop-types";

class Countdown extends Component {
  constructor(props) {
    super(props);

    this.state = {
      countdown: 0
    };
  }

  static propTypes = {
    seconds: PropTypes.number.isRequired,
    action: PropTypes.func.isRequired
  }

  tick() {
    if (this.state.countdown !== 0) {
      this.setState(prevState => ({
        countdown: prevState.countdown? prevState.countdown - 1 : 0
      }));
    }
  }

  componentDidUpdate() {
    if (this.state.countdown === 0) {
      this.props.action();
    }
  }

  componentDidMount() {
    this.interval = setInterval(() => this.tick(), 1000);
    this.setState({
      countdown: this.props.seconds
    });
  }

  componentWillUnmount() {
    clearInterval(this.interval);
  }

  render() {
    return (
      <span className="text-white">{this.state.countdown}</span>
    );
  }
}

export default Countdown;