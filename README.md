# OnePercent.IO - Assets for blockchain

[![Bitbucket build](https://img.shields.io/bitbucket/pipelines/stackchain/one-assets.svg)](https://bitbucket.org/stackchain/one-assets/addon/pipelines/home#!/)
[![GPLv3 license](https://img.shields.io/badge/License-GPLv3-blue.svg)](http://perso.crans.org/besson/LICENSE.html)
[![Bitbucket issues](https://img.shields.io/bitbucket/issues/stackchain/one-assets.svg)](https://bitbucket.org/stackchain/one-assets/issues/)
[![Analytics](https://ga-beacon.appspot.com/UA-104397498-2/bitbucket.org/stackchain/one-assets/src/master/README.md)](https://bitbucket.org/stackchain/one-assets/)

Welcome to OnePercent.IO assets for blockchain frictionless integration!

## Getting Started

1. If you are familiar with npm/yarn you are ready to go on, be aware that this project requires the API project.

## Contributing

We intend for this project to be an educational resource: we are excited to
share our wins, mistakes, and methodology of frictionless development as we work
in the open. Our primary focus is to continue improving the assets for our users in
line with our roadmap.

The best way to submit feedback and report bugs is to open a BitBucket issue.
Please be sure to include your operating system, device, version number, and
steps to reproduce reported bugs. Keep in mind that all participants will be
expected to follow our code of conduct.

## Code of Conduct

We aim to share our knowledge and findings as we work daily to improve our
product, for our community, in a safe and open space. We work as we live, as
kind and considerate human beings who learn and grow from giving and receiving
positive, constructive feedback. We reserve the right to delete or ban any
behavior violating this base foundation of respect.

[![Open Source Love png3](https://badges.frapsoft.com/os/v3/open-source.png?v=103)](https://github.com/ellerbrock/open-source-badges/)
