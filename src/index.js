import { h, render } from "preact";
import { RouteApp } from "./logic/routes";
import tracker from "./tracker";
if (process.env.NODE_ENV === "development") {
  require("preact/devtools");
}


tracker.setUp();

const internalRender = (options, el, merge) => render(<RouteApp {...options} />, el, merge);

const OneWebWidgets = {

  init: (options) => {
    console.log("one-widgets-sdk-version:", process.env.SDK_VERSION);

    tracker.track();

    const elementRoot = document.getElementById(options.element);
    const element = internalRender(options, elementRoot);

    return {
      element,
      options,

      setOptions(opt) {
        this.options = {...this.options, ...opt};
        internalRender(this.options, elementRoot, this.element);
        return this.options;
      },

      unmount() {
        render(null, elementRoot, this.element);
      }
    };
  }

};

export default OneWebWidgets;
