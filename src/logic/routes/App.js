import { h, Component } from "preact";
import { Provider } from "react-redux";

import mountStore from "../store";
import RouteWalletETH from "./WalletETH";
import RouteManagerKYC from "./ManagerKYC";
import { initializeI18n } from "../../view/locales";
import * as C from "../constants";

class RouteApp extends Component {

  constructor(props) {
    super(props);

    this.store = mountStore(props.hooks);
  }

  render () {
    const {
      language,
      component,
      externalUserId,
      externalSessionId,
      externalInvestmentId,
      readOnly
    } = this.props;

    switch (component) {
      case C.COMPONENT_WALLET:
        return (
          <Provider store={this.store}>
            <RouteWalletETH
              i18n={initializeI18n(language)}
              id={externalUserId}
              session={externalSessionId}
              readOnly={readOnly}
            />
          </Provider>
        );
      case C.COMPONENT_MANAGER_KYC:
        return (
          <Provider store={this.store}>
            <RouteManagerKYC
              i18n={initializeI18n(language)}
              id={externalInvestmentId}
              session={externalSessionId}
              readOnly={readOnly}
            />
          </Provider>
        );
      default:
        return (
          <span>ERROR: Invalid component.</span>
        );
    }
  }
}

RouteApp.defaultProps = {
  language: "en",
  component: "wallet",
  externalUserId: "",
  sessionSession: "",
  readOnly: true,
  hooks: []
};

export default RouteApp;