import { h, Component } from "preact";
import classNames from "classnames";

import { AssetOverlay as Overlay } from "../../assets";

class ManagerKYC extends Component {

  constructor(props) {
    super(props);
  }

  render () {
    const {
      readOnly,
      wallet,
      invalidWallet,
      onChangeField,
      onSaveWallet,
      overlay,
      i18n
    } = this.props;

    return (
      <div className="col-sm border rounded shadow-sm p-3">
        <form>
          <div className="form-group">
            <label for="wallet">{i18n.t("wallet.title")}</label>
            <input type="text" className={classNames({ "is-invalid": invalidWallet }, "form-control form-control-sm")} id="wallet" placeholder={i18n.t("wallet.placeholder")} value={wallet} disabled={readOnly} onChange={e => onChangeField(e.target.value)}/>
            <div className="invalid-feedback">{i18n.t("wallet.errors.invalid_wallet")}</div>
            <small id="walletHelp" class="form-text text-muted">i18n: Paste the wallet address here.</small>
          </div>
          <hr/>
          <div className="d-flex flex-row justify-content-end align-items-center">
            <button className="btn btn-primary btn-sm ml-1" type="button" disabled={readOnly || invalidWallet} onClick={onSaveWallet}>{i18n.t("wallet.button_save")}</button>
          </div>
        </form>
        <Overlay {...overlay} />
      </div>
    );
  }

}

export default ManagerKYC;